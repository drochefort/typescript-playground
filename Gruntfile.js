var pack = require('./package.json');

module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        tslint: {
            options: {
                configuration: 'tslint.json'
            },
            app: {
                src: ['src/**/*.ts']
            }
        },
        'typescript-formatter': {
            files: ['src/**/*.ts']
        },
        ts: {
            app: {
                outDir: 'js',
                src: ['src/**/*.ts']
            }
        },
        watch: {
            app: {
                options: {
                    livereload: true
                },
                tasks: ['ts'],
                files: [
                    'src/**/*.ts',
                    'css/*.css',
                    '*.html'
                ]
            }
        },
        connect: {
            app: {
                options: {
                    livereload: true,
                    port: 9500,
                    base: ['.']
                }
            }
        },
        open: {
            app: {
                path: 'http://localhost:9500'
            }
        }
    });

    grunt.registerTask('transpile', ['typescript-formatter', 'tslint', 'ts']);
    grunt.registerTask('default', ['transpile', 'connect', 'open', 'watch']);
}