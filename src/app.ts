interface Element {
    innerHTML: string;
};



(function(): void {
    'use strict';

    function greeter(person: string): string {
        return `Hello, ${person}!`;
    }

    const user: string = 'Jane Smith ccc';

    console.log('RELOADING', user);

    document.querySelector('.content').innerHTML = greeter(user);

})();
